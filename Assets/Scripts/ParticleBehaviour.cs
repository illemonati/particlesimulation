using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class ParticleBehaviour : MonoBehaviour {
    // Start is called before the first frame update

    public float mass = 0;
    public float charge = 0;
    public float K = (float) 8.9875517923e9;
    public float G = (float) 6.67408e-11;
    
    public Rigidbody2D selfRidgidBody;

    public event EventHandler ParticleSpawnHandler;
    private object _particlesSemaphore = new object();

    private Dictionary<int, ParticleBehaviour> _particles =
        new Dictionary<int, ParticleBehaviour>();


    public void Start() {
        
        
        selfRidgidBody = GetComponent<Rigidbody2D>();
        selfRidgidBody.mass = mass * Settings.ScaleFactor;

        Debug.Log(selfRidgidBody.mass);
        
        var particles = FindObjectsOfType(typeof(ParticleBehaviour)) as ParticleBehaviour[];
        lock (_particlesSemaphore) {
            if (particles == null) return;
            foreach (var particle in particles) {
                if (particle == this) continue;
                _particles[particle.GetInstanceID()] = particle;
                ParticleSpawnHandler += particle.OnParticleSpawn;
                // Debug.Log(particle.GetInstanceID());
            }
        }

        ParticleSpawnHandler?.Invoke(this, EventArgs.Empty);
    }

    void Update() {
        UpdateBasedOnOtherParticles();
    }

    public void OnParticleSpawn(object sender, EventArgs e) {
        if (!(sender is ParticleBehaviour senderParticle)) return;
        lock (_particlesSemaphore) {
            _particles[senderParticle.GetInstanceID()] = senderParticle;
        }
    }

    public float GetDistance(Vector3 a, Vector3 b) {
        return Mathf.Sqrt(Mathf.Pow((a.x - b.x), 2) + Mathf.Pow((a.y - b.y), 2) + Mathf.Pow((a.z - b.z), 2));
    }

    public void UpdateBasedOnOtherParticles() {
        lock (_particlesSemaphore) {
            foreach (var particle in _particles.Values) {
                var particleTransform = particle.transform;
                var particlePosition = particleTransform.position;
                var selfTransformPosition = transform.position;

                var rSquared = Mathf.Pow(GetDistance(selfTransformPosition, particlePosition), 2);
                
                
                var electricNorm = K * this.charge * particle.charge / rSquared;
                var scaledElectricNorm = electricNorm * Settings.ScaleFactor;

                // var gravityNorm = G * this.mass * particle.mass / rSquared;
                // var scaledGravityNorm = gravityNorm * Settings.ScaleFactor;

                // Debug.Log($"Name: {this.name}, Electric Norm: {electricNorm}, Gravity Norm: {gravityNorm}, Mass: {mass}");
              


                var direction = (selfTransformPosition - particlePosition).normalized;
                selfRidgidBody.AddForce(Time.deltaTime * scaledElectricNorm * direction, ForceMode2D.Force);
                // selfRidgidBody.AddForce(direction * scaledGravityNorm);
            }
        }
    }
}